import re
import hashlib
import json
import Users_search





def landing_page():
        print("Hello, this is a testing platform!\nPlease select the following: \n1. Log in\n2. Sign up\n3. Exit")
        selection = int(input("Select the number: "))
        count = 0
        if selection == 1:
                print("Please enter your details: ")
                login_1 = input("Login: ")
                password_1 = input("Password: ")
                x = hashlib.md5(login_1.encode())
                y = hashlib.md5(password_1.encode())
                login = x.hexdigest()
                password = y.hexdigest()
                with open("admin_login_details.txt", 'r') as r:
                        access_admin_login = r.read()
                with open("user_login_details.txt", 'r') as users_r:
                        access_users_login = users_r.readlines()
                        z = json.loads(access_admin_login)
                        adm_log = z['login']
                        adm_pas = z['password']
                        if login == adm_log and password == adm_pas:
                                count = 1
                        else:
                                for i in access_users_login:
                                        u = json.loads(i)
                                        user_log = u['login']
                                        user_pas = u['password']
                                        if login == user_log and password == user_pas:
                                                count = 2
                r.close()
                users_r.close()

                if count == 0:
                        print("User does not exist, please check your details or sing up")
                        landing_page()
                        quit()
                elif count == 1:
                        print("Welcome back admin") # add admin func here
                        Users_search.Admin_menu(adm_log,adm_pas)
                elif count == 2:
                        print("Welcome back user") # add user func here


        elif selection == 2:
                        print("Please enter the following details to sing up!")
                        m = True
                        while m is True:
                                user_login = str(input("Login: "))
                                x = hashlib.md5(user_login.encode())
                                enc_user_login = x.hexdigest()
                                if len(user_login) >= 12 or len(user_login) < 6:
                                        print("Login must be from 6 to 10 characters")
                                elif re.search('[0-9]', user_login) is None:
                                        print("Make sure your login has a number in it")
                                elif re.search('[A-Z]', user_login) is None:
                                        print("Make sure your login has a capital letter in it")
                                else:
                                        with open("admin_login_details.txt", 'r') as r:
                                                access_admin_login = r.read()
                                        with open("user_login_details.txt", 'r') as users_r:
                                                access_users_login = users_r.readlines()
                                                z = json.loads(access_admin_login)
                                                adm_log = z['login']
                                        if enc_user_login == adm_log:
                                                print("This login already exists, please choose another login!")
                                        elif enc_user_login != adm_log:
                                                if len(access_users_login) == 0:
                                                        m = False
                                                        print("Login accepted")
                                                        r.close()
                                                        users_r.close()
                                                else:
                                                        for i in access_users_login:
                                                                g = json.loads(i)
                                                                if enc_user_login != g['login']:
                                                                        x = 1
                                                                        continue
                                                                elif enc_user_login == g['login']:
                                                                        print("This login already exists, please choose another login!")
                                                                        x = 2
                                                                        break
                                                        if x == 1:
                                                                m = False
                                                                print("Login accepted")
                                                                r.close()
                                                                users_r.close()
                                                        else:
                                                                m = True

                        while True:
                                user_password = input("Enter a password: ")
                                if len(user_password) < 8 or len(user_password) > 12:
                                        print("Make sure your password is at lest 8 letters and no more than 12")
                                elif re.search('[0-9]', user_password) is None:
                                        print("Make sure your password has a number")
                                elif re.search('[A-Z]', user_password) is None:
                                        print("Make sure your password has a capital letter")
                                else:
                                        print("Password accepted")
                                        break

                        y = hashlib.md5(user_password.encode())
                        enc_user_password = y.hexdigest()
                        user_fullname = str(input("Enter full name: "))
                        user_address = str(input("Enter address: "))
                        user_tel = int(input("Enter telephone number: +"))  # Зроби щоб не вилітало, якщо не ввів цифру, ок?
                        user_details = {"login": enc_user_login, "password": enc_user_password, "full_name": user_fullname, "address": user_address, "tel": user_tel}

                        with open("user_login_details.txt", 'a+') as h:
                                h.seek(0)
                                data = h.read()
                                if len(data) > 0:
                                        h.write("\n")
                                h.write(json.dumps(user_details))
                                h.close()
                                print("User has been registered on the testing platform.")
                                landing_page()

        elif selection == 3:
                        print("Good bye!")
                        quit()


def first_entry():
        with open("admin_login_details.txt", 'r') as l:
                if len(l.readline()) == 0:
                        print("Hello admin, this is your first entry to the program.")
                        print("Please create login and password")
                        while True:
                                login = input("Login: ")
                                if len(login) >= 12 or len(login) < 6:
                                        print("Login must be from 6 to 10 characters")
                                elif re.search('[0-9]', login) is None:
                                        print("Make sure your login has a number in it")
                                elif re.search('[A-Z]', login) is None:
                                        print("Make sure your login has a capital letter in it")
                                else:
                                        print("Login accepted!")
                                        break

                        while True:
                                password = input("Enter a password: ")
                                if len(password) < 8 or len(password) > 12:
                                        print("Make sure your password is at lest 8 letters and no more than 12")
                                elif re.search('[0-9]', password) is None:
                                        print("Make sure your password has a number")
                                elif re.search('[A-Z]', password) is None:
                                        print("Make sure your password has a capital letter")
                                else:
                                        print("Password accepted")
                                        break

                        x = hashlib.md5(login.encode())
                        y = hashlib.md5(password.encode())
                        admin_password = y.hexdigest()
                        admin_login = x.hexdigest()
                        admin_details = {'login': admin_login, 'password': admin_password}

                        with open("admin_login_details.txt", 'w') as l:
                            l.write(json.dumps(admin_details))
                        l.close()
                        print("Admin has been registered on the testing platform.")
                        Users_search.Admin_menu(admin_login,admin_password)
                else:
                        landing_page()

first_entry()




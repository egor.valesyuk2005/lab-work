import re
import hashlib
import json
import login_func



def Users_add():
    print("Please enter the following details to sing up!")
    m = True
    while m is True:
        user_login = str(input("Login: "))
        x = hashlib.md5(user_login.encode())
        enc_user_login = x.hexdigest()
        if len(user_login) >= 12 or len(user_login) < 6:
            print("Login must be from 6 to 10 characters")
        elif re.search('[0-9]', user_login) is None:
            print("Make sure your login has a number in it")
        elif re.search('[A-Z]', user_login) is None:
            print("Make sure your login has a capital letter in it")
        else:
            with open("admin_login_details.txt", 'r') as r:
                access_admin_login = r.read()
            with open("user_login_details.txt", 'r') as users_r:
                access_users_login = users_r.readlines()
                z = json.loads(access_admin_login)
                adm_log = z['login']
            if enc_user_login == adm_log:
                print("This login already exists, please choose another login!")
            elif enc_user_login != adm_log:
                if len(access_users_login) == 0:
                    m = False
                    print("Login accepted")
                    r.close()
                    users_r.close()
                else:
                    for i in access_users_login:
                        g = json.loads(i)
                        if enc_user_login != g['login']:
                            x = 1
                            continue
                        elif enc_user_login == g['login']:
                            print("This login already exists, please choose another login!")
                            x = 2
                            break
                    if x == 1:
                        m = False
                        print("Login accepted")
                        r.close()
                        users_r.close()
                    else:
                        m = True

    while True:
        user_password = input("Enter a password: ")
        if len(user_password) < 8 or len(user_password) > 12:
            print("Make sure your password is at lest 8 letters and no more than 12")
        elif re.search('[0-9]', user_password) is None:
            print("Make sure your password has a number")
        elif re.search('[A-Z]', user_password) is None:
            print("Make sure your password has a capital letter")
        else:
            print("Password accepted")
            break

    y = hashlib.md5(user_password.encode())
    enc_user_password = y.hexdigest()
    user_fullname = str(input("Enter full name: "))
    user_address = str(input("Enter address: "))
    user_tel = int(input("Enter telephone number: +"))
    user_details = {"login": enc_user_login, "password": enc_user_password, "full_name": user_fullname,
                    "address": user_address, "tel": user_tel}

    with open("user_login_details.txt", 'a+') as h:
        h.seek(0)
        data = h.read()
        if len(data) > 0:
            h.write("\n")
        h.write(json.dumps(user_details))
        h.close()
        print("User has been registered on the testing platform.")

def change_admin_info(admLogin, admPassword):
        print("1. Change Login\n"
              "2. Change password")
        ans = int(input("Choose action: "))
        if ans == 1:
            while True:
                login = input("Login: ")
                if len(login) >= 12 or len(login) < 6:
                    print("Login must be from 6 to 10 characters")
                elif re.search('[0-9]', login) is None:
                    print("Make sure your login has a number in it")
                elif re.search('[A-Z]', login) is None:
                    print("Make sure your login has a capital letter in it")
                else:
                    print("Login accepted!")
                    break
            x = hashlib.md5(login.encode())
            admin_login = x.hexdigest()
            admin_details = {'login': admin_login, 'password': admPassword}
        if ans == 2:
            while True:
                password = input("Enter a password: ")
                if len(password) < 8 or len(password) > 12:
                    print("Make sure your password is at lest 8 letters and no more than 12")
                elif re.search('[0-9]', password) is None:
                    print("Make sure your password has a number")
                elif re.search('[A-Z]', password) is None:
                    print("Make sure your password has a capital letter")
                else:
                    print("Password accepted")
                    break
            y = hashlib.md5(password.encode())
            admin_password = y.hexdigest()
            admin_details = {'login': admLogin, 'password': admin_password}



        with open("admin_login_details.txt", 'w') as l:
            l.write(json.dumps(admin_details))
        l.close()
        print("Admin information has been changed")

def Admin_menu(admLogin, admPassword):
    print("1. Change login or password\n"
          "2. Change Users information\n"
          "3. Look statistics\n"
          "4. Change Tests\n"
          "5. Exit")
    ans = int(input("Choose action: "))
    if ans == 1:
        change_admin_info(admLogin,admPassword)
        Admin_menu(admLogin, admPassword)
    elif ans == 2:
        User_change_menu(admLogin,admPassword)
        Admin_menu(admLogin,admPassword)
    elif ans ==3:
        # def tabl_stat
        Admin_menu(admLogin,admPassword)
        pass
    elif ans == 4:
        # def tabl_chnage()
        Admin_menu(admLogin,admPassword)
        pass
    elif ans == 5:
        quit()
    else:
        print("Incorrect information")
        Admin_menu(admLogin,admPassword)




def User_change_menu(admLogin,admPassword):
    print("1. Add user\n"
          "2. Delete user\n"
          "3. Change user information")
    ans = int(input("Choose action: "))
    if ans == 1:
        Users_add()
        pass
    elif ans == 2:
        User_delete(admLogin,admPassword)
    elif ans == 3:
        User_Change_inf(admLogin,admPassword)
    else:
        print("Incorrect info")
        User_change_menu(admLogin,admPassword)

def User_delete(admLogin,admPassword):
    dellogin = input("User login: ")
    x = hashlib.md5(dellogin.encode())
    user_login = x.hexdigest()
    with open("user_login_details.txt","r") as f:
        l=[]
        k=0
        for log in f:
            d = json.loads(log)

            if d.setdefault("login") != user_login:
                l.append(log)
            else:
                print("User deleted")
                k=1

        if k==0:
            print("Login is incorrect")
            Admin_menu(admLogin,admPassword)
    with open("user_login_details.txt","w") as f:
        for i in l:
            f.write(i)
    f.close()



def User_Change_inf(admLogin,admPassword):
    oldLogin = input("User change info login: ")
    x = hashlib.md5(oldLogin.encode())
    oldLogin = x.hexdigest()
    with open("user_login_details.txt", "r") as f:
        l = []
        k = 0
        for lines in f:
            d = json.loads(lines)

            if d.setdefault('login') == oldLogin:
                print("User information: "+str(d))
                login = oldLogin
                password = d.setdefault('password')
                FIO = d.setdefault('full_name')
                address = d.setdefault('address')
                phone = d.setdefault('tel')
                print("1. Change login\n"
                      "2. Change password\n"
                      "3. Change full name\n"
                      "4. Change adress\n"
                      "5. Change phone\n"
                      "6. Back")
                ans = int(input("Choose action: "))
                if ans == 1:
                    newLogin = input("New login: ")
                    x = hashlib.md5(newLogin.encode())
                    newLogin = x.hexdigest()
                    newuser = {"login": newLogin, "password": password, "full_name": FIO, "address": address,"tel": phone}
                elif ans == 2:
                    newPassword = input("New password: ")
                    x = hashlib.md5(newPassword.encode())
                    newPassword = x.hexdigest()
                    newuser = {"login": login, "password": newPassword, "full_name": FIO, "address": address,"tel": phone}
                elif ans == 3:
                    newFIO = input("New full name: ")
                    newuser = {"login": login, "password": password, "full_name": newFIO, "address": address,
                               "tel": phone}
                elif ans == 4:
                    newAddress = input("New address: ")
                    newuser = {"login": login, "password": password, "full_name": FIO, "address": newAddress,
                               "tel": phone}
                elif ans == 5:
                    newPhone = input("New phone: +")
                    newuser = {"login": login, "password": password, "full_name": FIO, "address": address,
                               "tel": newPhone}
                elif ans == 6:
                    newuser = {"login": login, "password": password, "full_name": FIO, "address": address,
                               "tel": phone}
                d = json.dumps(newuser)
                l.append(d)
                k = 1
            else:
                l.append(lines)
        if k == 0:
            print("Login is incorrect")
            Admin_menu(admLogin,admPassword)
    with open("user_login_details.txt", "w") as f:
        for i in l:
            f.write(str(i))
    f.close()
    login_func.landing_page()

